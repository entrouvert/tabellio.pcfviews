from Products.CMFCore.utils import getToolByName
from Products.Five import BrowserView

class View(BrowserView):
    def related_search_url(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            form_url = catalog(
                portal_type='Folder',
                object_provides=('tabellio.searchform.form.IFolderWithPfbDocuments',
                                 'tabellio.searchform.form.IFolderWithDocuments'),
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return None
        return '%s/?dossier.widgets.sort_on:list=Session&dossier.widgets.search_type_is_dossier=1&dossier.widgets.topics=%s' % (form_url, ' '.join(self.context.topics))

