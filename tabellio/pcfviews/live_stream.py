# -*- coding: utf-8 -*-

from Products.CMFCore.utils import getToolByName
from datetime import datetime
import logging

logger = logging.getLogger("Plone")

STREAM_LOCATIONS = {
    u"HG Salle Bleue": "http://audio.pfwb.be:8007/",
    u"HG Salle des Gouverneurs": "http://audio.pfwb.be:8006/",
    u"HL Salle Audiovisuelle": "http://audio.pfwb.be:8002/audio",
    u"HL Salle Blanche": "http://audio.pfwb.be:8003/blanche",
    u"HL Salle Ovale": "http://audio.pfwb.be:8004/ovale",
    u"HL Salle du Parc": "http://audio.pfwb.be:8001/parc",
    u"H\xe9micycle": "http://audio.pfwb.be:8005/hemi",
}


class Stream(object):

    def __init__(self, location, start, end, real_finished):
        self.location = location
        self.start = start
        self.end = end
        self.real_finished = real_finished

    @property
    def enabled(self):
        return (self.location in STREAM_LOCATIONS)

    @property
    def streaming(self):
        return (self.enabled and not self.not_yet_streaming and not self.finished)

    @property
    def not_yet_streaming(self):
        now = datetime.now()
        return (self.enabled and now < self.start)

    @property
    def finished(self):
        now = datetime.now()
        return (self.enabled and now > self.end and self.real_finished)

    @property
    def url(self):
        return STREAM_LOCATIONS.get(self.location)


def get_live_stream_infos_for_context(context):
    # event_id = context.id
    # portal = getToolByName(context, "portal_url").getPortalObject()
    # db_connection = portal.db._wrapper.connection
    # cursor = db_connection.cursor()
    # cursor.execute('''SELECT lieu, datedeb, heuredeb, datefin, heurefin, finreel
    #                   FROM t_reunion
    #                   WHERE id = %(id)s''', {'id': event_id})
    # row = cursor.fetchone()
    # cursor.close()
    # lieu, datedeb, heuredeb, datefin, heurefin, finreel = row

    # -- For testing purposes
    from datetime import date
    from datetime import time
    lieu, datedeb, heuredeb, datefin, heurefin, finreel = (
        "HG Salle Bleue",
        date(2019, 9, 1),
        time(9, 0),
        date(2019, 9, 2),
        time(9, 0),
        True,
    )

    start = datetime.combine(datedeb, heuredeb)
    end = datetime.combine(datefin, heurefin)
    stream_infos = Stream(lieu, start, end, finreel)
    return stream_infos
