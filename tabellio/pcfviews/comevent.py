from five import grok
from Products.CMFCore.utils import getToolByName
from parlevent import ParlEventBaseView
from tabellio.agenda.comevent import IComEvent
import tabellio.config.utils

class CommissionView(ParlEventBaseView):
    def get_stream_name(self):
        if not self.context.commission:
            return 'XXXX'
        com_id = self.context.commission.to_path.split('/')[-1]
        com_audio_code = tabellio.config.utils.get_com_audio_code(com_id)
        filename = '%04d%02d%02d%02d-%s.mp3' % (
                        self.context.start.year, self.context.start.month,
                        self.context.start.day, self.context.start.hour,
                        com_audio_code)
        return filename

class View(grok.View, CommissionView):
    grok.context(IComEvent)
    grok.require('zope2.View')

class M3U(grok.View, CommissionView):
    grok.context(IComEvent)
    grok.require('zope2.View')
    grok.name('ecouter.m3u')

    def render(self):
        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()
        self.request.response.setHeader('Content-type', 'audio/x-mpegurl')
        return portal_url + '/mp3/jp/' + self.get_stream_name() + '\n'
