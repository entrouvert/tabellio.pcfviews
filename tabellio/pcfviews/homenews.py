from five import grok
from zope import schema
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice
from Products.Five import BrowserView

from plone.directives import form, dexterity
from plone.dexterity.content import Item
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.app.textfield import RichText
from plone.namedfile.field import NamedImage

from tabellio.pcfviews import MessageFactory as _

class IHomeNews(form.Schema):
    picture = NamedImage(title=_(u'Picture'), required=False)
    homepage_text = RichText(title=_(u'Homepage Text'), required=True)
    text = RichText(title=_(u'Text'), required=False)
    on_homepage = schema.Bool(title=_(u'Include on homepage'), default=True)

class HomeNews(Item):
    implements(IHomeNews)

class View(grok.View):
    grok.context(IHomeNews)
    grok.require('zope2.View')

class PictureView(BrowserView):
    def __call__(self):
        if not self.context.picture:
            return None
        self.request.response.setHeader('Content-type', self.context.picture.contentType)
        return self.context.picture.data

