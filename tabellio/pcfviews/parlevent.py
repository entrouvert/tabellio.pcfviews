import urllib2

from five import grok
from zope import schema, component
from Products.CMFCore.utils import getToolByName

from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

import tabellio.agenda.parlevent

from tabellio.pcfviews.live_stream import get_live_stream_infos_for_context


class ParlEventBaseView(tabellio.agenda.parlevent.ParlEventBaseView):

    def get_live_stream_infos(self):
        return get_live_stream_infos_for_context(self.context)

    def has_stream(self):
        return (self.get_stream_url() is not None)

    def get_stream_url(self):
        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()
        stream_url = portal_url + '/mp3/jp/' + self.get_stream_name()
        try:
            response = urllib2.urlopen(stream_url)
            if response.code == 200:
                return stream_url
        except:
            pass
        return None


class ParlEventView(ParlEventBaseView):

    def get_stream_name(self):
        filename = '%04d%02d%02d%02d-SEAN.mp3' % (
                        self.context.start.year, self.context.start.month,
                        self.context.start.day, self.context.start.hour)
        return filename


class View(grok.View, ParlEventView):
    grok.context(tabellio.agenda.parlevent.IParlEvent)
    grok.require('zope2.View')

class M3U(grok.View, ParlEventView):
    grok.context(tabellio.agenda.parlevent.IParlEvent)
    grok.require('zope2.View')
    grok.name('ecouter.m3u')

    def render(self):
        portal_url = getToolByName(self.context, 'portal_url').getPortalObject().absolute_url()
        self.request.response.setHeader('Content-type', 'audio/x-mpegurl')
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if self.context.is_now():
            return settings.live_stream_url + '\n'
        return portal_url + '/mp3/jp/' + self.get_stream_name() + '\n'
