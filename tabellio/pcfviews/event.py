from five import grok

from tabellio.agenda.event import EventBaseView, IEvent

class View(grok.View, EventBaseView):
    grok.context(IEvent)
    grok.require('zope2.View')

    def next_event_url(self):
        return EventBaseView.next_event_url(self)

    def previous_event_url(self):
        return EventBaseView.previous_event_url(self)
