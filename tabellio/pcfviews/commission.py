# -*- coding: utf-8 -*-

from zope import component
from zc.relation.interfaces import ICatalog
from zope.app.intid.interfaces import IIntIds
from Products.CMFCore.utils import getToolByName

from Products.Five import BrowserView

from themis.fields.vocabs import cmp_person

class View(BrowserView):
    def presidence_polgroups(self):
        return [self.context.president.to_object.polgroup.to_object]

    def presidence_members(self, polgroup):
        return [self.context.president.to_object]

    def list_polgroups(self, list):
        d = {}
        polgroups = {}
        for member in list:
            polgroup = member.to_object.polgroup.to_object
            polgroup_id = polgroup.id
            polgroups[polgroup.id] = polgroup
            if not polgroup_id in d:
                d[polgroup_id] = 0
            d[polgroup_id] = d[polgroup_id] + 1
        items = d.items()
        items.sort(lambda x,y: cmp(x[1], y[1]))
        items.reverse()
        return [polgroups.get(x[0]) for x in items]

    def vicepresidence_polgroups(self):
        return [x.to_object.polgroup.to_object for x in self.context.vicepresidents]

    def vicepresidence_members(self, polgroup):
        return sorted([x.to_object for x in self.context.vicepresidents
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)

    def members_polgroups(self):
        return self.list_polgroups(self.context.members)

    def members_members(self, polgroup):
        return sorted([x.to_object for x in self.context.members
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)

    def substitutes_polgroups(self):
        return self.list_polgroups(self.context.substitutes)

    def substitutes_members(self, polgroup):
        return sorted([x.to_object for x in self.context.substitutes
                       if x.to_object.polgroup.to_object == polgroup], cmp_person)

    def get_meetings(self):
        intids = component.getUtility(IIntIds)
        catalog = component.getUtility(ICatalog)
        try:
            com_intid = intids.getId(self.context)
        except KeyError:
            intids.register(self.context)
            com_intid = intids.get(self.context)
        # objects created from relations do not have a working absolute_url()
        # method; so we get ids and go through the catalog do fetch appropriate
        # brains.
        meeting_ids = [x.from_object.id for x in catalog.findRelations(
                        {'to_id': com_intid, 'from_attribute': 'commission'})]
        portal_catalog = getToolByName(self.context, 'portal_catalog')
        return portal_catalog(portal_type='tabellio.agenda.comevent',
                              id=meeting_ids,
                              sort_on='start', sort_order='descending')[:10]

    def current_dossiers(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        brains = list(catalog(commissionsDoc=self.context.id))
        brains.sort(lambda x,y: -cmp(x.dateDoc, y.dateDoc))
        return brains
